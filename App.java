import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception {
        Scanner input = new Scanner(System.in);
        int pilih, cel;
        System.out.println("Program konversi suhu");
        System.out.println("menu");
        System.out.println("1. celcius");
        System.out.println("2. fahrenheit");
        System.out.print("pilih :");
        pilih = input.nextInt();


        switch (pilih) {
            case 1:
                //panggi  class
                System.out.print("Masukan celcius : ");
                cel = input.nextInt();

                konversi kvs = new konversi(cel);
                System.out.println("jadi " + cel + " celcius sama dengan " + kvs.celcius() + " fahrenheit");
                break;
            case 2:
                //panggi class
                System.out.print("Masukan fahrenheit : ");
                cel = input.nextInt();

                konversi kvs2 = new konversi(cel);
                System.out.println("jadi " + cel + " fahrenheit sama dengan " + kvs2.fahrenheit() + " celcius");
                break;
        
            default:
                System.out.println("silakan pilih sesuai dengan yang tertera");
                break;
        }
        
        input.close();
    }
}
