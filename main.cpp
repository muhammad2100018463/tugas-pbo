#include <iostream>
using namespace std;

class Konversi
{
private:
    int hasil;

public:
    int celcius(int cel);
    int fahrenheit(int cel);
};

int Konversi::celcius(int cel)
{
    return hasil = (cel * 9 / 5) + 32;
}

int Konversi::fahrenheit(int cel)
{
    return hasil = (cel - 32) * 5 / 9;
}

int main()
{
    Konversi kvs;
    int pilih, cel;

    cout << "program konversi suhu" << endl;
    cout << "Menu " << endl;
    cout << "1. celcius " << endl;
    cout << "2. fahrenheit " << endl;
    cout << "pilih : ";
    cin >> pilih;

    if (pilih == 1)
    {
        cout << "Masukan celcius : ";
        cin >> cel;
        cout << "jadi " << cel << " celcius sama dengan " << kvs.celcius(cel) << " fahrenheit";
    }
    else if (pilih == 2)
    {
        cout << "Masukan fahrenheit : ";
        cin >> cel;
        cout << "jadi " << cel << " fahrenheit sama dengan " << kvs.fahrenheit(cel) << " fahrenheit";
    }
    else
    {
        cout << "silakan pilih sesuai menu";
    }

    return 0;
}